=== Very Simple Contact Form ===
Contributors: Guido07111975
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=donation%40guidovanderleest%2enl
Version: 6.7
License: GNU General Public License v3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Requires at least: 3.7
Tested up to: 4.8
Stable tag: trunk
Tags: simple, contact, form, contact form, email


This is a very simple contact form. Use shortcode [contact] to display form on page or use the widget.


== Description ==
= About =
This is a very simple responsive translatable contact form.

Form only contains Name, Email, Subject and Message. And a simple captcha sum.

It's designed to fulfill the needs of most websites that only require a basic contact form, with no additional fields.

= How to use =
After installation add shortcode `[contact]` on your page to display the form.

Or go to Appearance > Widgets and add the widget to your sidebar.

By default form submissions will be send to your site's admin email address (set in Settings > General). It's also possible to send a confirmation to the sender.

While adding the shortcode or the widget you can add several attributes to personalize your form.

While adding the widget you can add some additional information above your form.

= Shortcode attributes = 
* Change email from admin: `[contact email_to="your-email-here"]`
* Multiple email: `[contact email_to="first-email-here, second-email-here"]`
* Change default "From" header: `[contact from_header="your-email-here"]`
* Activate confirmation to sender: `[contact auto_reply="true"]`
* Change default confirmation message: `[contact auto_reply_message="your message here"]`
* Hide subject field: `[contact hide_subject="true"]`
* Add custom subject to mail: `[contact subject="your subject here"]`

You can also change labels and messages using an attribute.

* Labels: label_name, label_email, label_subject, label_captcha, label_message, label_submit
* Labels in case of error: error_name, error_email, error_subject, error_captcha, error_message
* Sending succeeded: message_success
* Sending failed: message_error

Examples:

* Change Name and Submit labels: `[contact label_name="Your Name" label_submit="Send"]`
* Change captcha label: `[contact label_captcha="Please enter %s"]`
* Change captcha label: `[contact label_captcha="Please enter %s here"]`
* Change sending succeeded message: `[contact message_success="your message here"]`

= Widget attributes =
The widget supports the same attributes. Enter them without shortcode itself and without brackets.

Examples:

* Change email from admin: `email_to="your-email-here"`
* Multiple email: `email_to="first-email-here, second-email-here"`
* Change default "From" header: `from_header="your-email-here"`
* Activate confirmation to sender: `auto_reply="true"`
* Change default confirmation message: `auto_reply_message="your message here"`
* Hide subject field: `hide_subject="true"`
* Add custom subject to mail: `subject="your subject here"`
* Change Name and Submit labels: `label_name="Your Name" label_submit="Send"`
* Change captcha label: `label_captcha="Please enter %s"`
* Change captcha label: `label_captcha="Please enter %s here"`
* Change sending succeeded message: `message_success="your message here"`

= List form submissions in dashboard =
With plugin [Contact Form DB](https://wordpress.org/plugins/contact-form-7-to-database-extension) you can list form submissions in your dashboard.

Note: Contact Form DB is currently only available on [GitHub](https://github.com/mdsimpson/contact-form-7-to-database-extension/releases).

= SMTP =
SMTP (Simple Mail Transfer Protocol) is an internet standard for sending emails. 

WordPress supports the PHP mail() function by default, but when using SMTP there's less chance your form submissions are being marked as spam.

You must install an additional plugin for this. I have tested my plugin with these SMTP plugins:

* [Gmail SMTP](https://wordpress.org/plugins/gmail-smtp/)
* [Easy WP SMTP](https://wordpress.org/plugins/easy-wp-smtp/)
* [WP mail SMTP](https://wordpress.org/plugins/wp-mail-smtp/)
* [Postman SMTP](https://wordpress.org/plugins/postman-smtp/)

= Question? =
Please take a look at the FAQ section.

= Translation =
Not included but plugin supports WordPress language packs.

More [translations](https://translate.wordpress.org/projects/wp-plugins/very-simple-contact-form) are very welcome!

= Credits =
Without the WordPress codex and help from the WordPress community I was not able to develop this plugin, so: thank you!

Enjoy!


== Installation ==
Please check Description section for installation info.


== Frequently Asked Questions ==
= Where is the settingspage? =
Plugin has no settingspage, use a shortcode with attributes or the widget with attributes to make it work.

= How do I set plugin language? =
Plugin uses the WP Dashboard language, set in Settings > General.

If plugin language pack is not available, language fallback will be English.

= How do I add attributes? =
You can find more info about this at the Description section.

= How do I style my form? =
It mostly depends on the stylesheet of your theme.

You can change style (CSS) using for example the [Very Simple Custom Style](https://wordpress.org/plugins/very-simple-custom-style) plugin.

= Does sender receive a confirmation after submitting form? =
Yes, this is possible.

You can find more info about this at the Description section.

= What do you mean with confirmation message? =
If you've activated the confirmation mail to sender, a message will be added to this mail.

The default confirmation message is the same as the default message that is displayed after submitting the form.

= Why is the "from" email not from sender? =
I have used a default so called "From" header to avoid form submissions being marked as spam.

Best practice is using a "From" header (an email address) that ends with your site domain.

That's why the default "From" header starts with "wordpress" and ends with your site domain.

Your reply to sender will use another header, called "Reply-To", which is the email address that sender has filled in.

= Can I change the "From" header? =
Yes, this is possible.

You can find more info about this at the Description section.

= Can I change the mail subject? =
Yes, this is possible.

You can find more info about this at the Description section.

Note: this subject will also be used in the confirmation to sender (if activated).

= Can I hide Subject field? =
Yes, this is possible.

You can find more info about this at the Description section.

= Can user enter HTML in form? =
Yes, save HTML is allowed in message field and widget info field.

= Can I use multiple shortcodes? =
Do not use multiple shortcodes on the same website. This might cause a conflict.

But you can use the shortcode on a page and the widget on the same website.

= Are form submissions listed in my dashboard? =
No, my plugin only sends form submissions to the email address of your choice.

With plugin [Contact Form DB](https://wordpress.org/plugins/contact-form-7-to-database-extension) you can list form submissions in your dashboard.

Note: Contact Form DB is currently only available on [GitHub](https://github.com/mdsimpson/contact-form-7-to-database-extension/releases).

= Why does form submission fail? =
An error message is displayed if plugin was unable to send form. This might be a server issue.

Your hosting provider might have disabled the PHP mail() function of your server. Ask them for more info about this.

They might advice you to install a SMTP plugin.

You can find more info about this at the Description section.

= Why am I not receiving form submissions? =
* Look also in your junk/spam folder.
* Check the Description section above and check shortcode (attributes) for mistakes.
* Install another contact form plugin (such as Contact Form 7) to determine whether it's caused by my plugin or something else.
* In case you're using a SMTP plugin, please check their settingspage for mistakes.

= Why does the captcha number not display properly? =
The captcha (random number) uses a php session to temporary store the number and some hostingproviders have disabled the use of sessions. Ask them for more info about this.

= Does this plugin has anti-spam features? =
Of course, the default WordPress sanitization and escaping functions are included.

It also contains 2 (invisible) honeypot fields (firstname and lastname) and a simple captcha sum.

= How can I make a donation? =
You like my plugin and you're willing to make a donation? Nice! There's a PayPal donate link on the WordPress plugin page and my website.

= Other question or comment? =
Please open a topic in plugin forum.


== Changelog ==
= Version 6.7 =
* new: shortcode attribute to add custom subject to mail: subject
* for more info please check readme file
* updated readme file

= Version 6.6 =
* new: sender can receive a confirmation by mail after submitting form
* added 2 shortcode attributes: auto_reply and auto_reply_message
* for more info please check readme file
* updated files vscf-form and vscf-widget-form
* added info in readme file about the use of smtp plugins
* updated FAQ

= Version 6.5 =
* changed the "from" header again to avoid form submissions being marked as spam
* the "from" header is now a default email that ends with your site domain
* also added new shortcode attribute to change this email again: from_header
* for more info please check readme file
* updated files vscf, vscf-form and vscf-widget-form
* updated readme file
* thanks again Sanjay Ojha and John, much appreciated

= Version 6.4 =
* changed the "from" header to avoid form submissions being marked as spam
* the "from" header is now email from admin
* updated files vscf-form and vscf-widget-form
* thanks Sanjay Ojha and John

For all versions please check file changelog.


== Screenshots == 
1. Very Simple Contact Form (Twenty Seventeen theme).
2. Very Simple Contact Form (Twenty Seventeen theme).
3. Very Simple Contact Form widget (Twenty Seventeen theme).
4. Very Simple Contact Form widget (dashboard).