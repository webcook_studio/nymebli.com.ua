��    &      L      |      |     }     �     �     �     �     �     �       '   +  +   S          �     �     �  8   �  4     ;   6     r     {  &   �     �     �     �  	   �     	       6   *     a     �     �     �     �     �       =        [  
   h  �  s  (   q	  !   �	  !   �	     �	     �	  #   
  (   <
     e
  6   �
  1   �
  (   �
          .  "   D  D   g  D   �  D   �     6  (   L  D   u     �     �  -   �           5  (   J  .   s  (   �  (   �  (   �  (     (   F  $   o     �  D   �  $   �        "Add to Cart" button "Add to Cart" icon "Add to Cart" text "Add to Wishlist" icon "Add to Wishlist" text "Add to wishlist" button "Browse wishlist" text "Product added" text Add an icon to the "Add to Cart" button Add an icon to the "Add to Wishlist" button Add second remove button Add to Cart Add to Wishlist After "Add to cart" An error occurred while adding products to the wishlist. An error occurred while adding products to wishlist. An error occurred while removing products from the wishlist In Stock My wishlist on %s No products were added to the wishlist Product Name Product correctly added to cart Product successfully removed. Share on: Shared Show "Add to Cart" button Show "Add to Cart" button for each product in wishlist Show "Pin on Pinterest" button Show "Share by Email" button Show "Share on Facebook" button Show "Share on Google+" button Show "Tweet on Twitter" button Show Stock status Show Unit price Show the date when users have added a product to the wishlist Stock Status Unit Price Project-Id-Version: YITH WooCommerce Wishlist
POT-Creation-Date: 2017-05-11 15:57+0200
PO-Revision-Date: 2017-08-28 09:30+0000
Last-Translator: admin <webcook.info@gmail.com>
Language-Team: Ukrainian
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco - https://localise.biz/
X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;__ngettext:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: .
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2)
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPathExcluded-0: plugin-fw
Report-Msgid-Bugs-To:  "Додати в кошик" button text "Додати в кошик" icon "Додати в кошик" text "До Побажань" icon "До Побажань" text "До Побажань" button text "Додати в кошик" button text Назва продукту Add an icon to the "Додати в кошик" button Add an icon to the "До Побажань" button "Додати в кошик" button text Додати в кошик До Побажань After "Додати в кошик" Немає доданих продуктів до вибраного Немає доданих продуктів до вибраного Немає доданих продуктів до вибраного в наявності Мій обраний список в %s Немає доданих продуктів до вибраного Назва продукту Назва продукту Товар успішно видалений. Поширити в: Поширити в: "Додати в кошик" button text "Додати в кошик" button background "Додати в кошик" button text "Додати в кошик" button text "Додати в кошик" button text "Додати в кошик" button text "Додати в кошик" button text Наявність на складі Ціна Немає доданих продуктів до вибраного Наявність на складі Ціна 