��          �      �        
   	  &        ;     A     Q     e     l     �     �     �     �     �  #   �  "   �     
     *     2  ;   9  �   u          
     #  w  B     �  *   �     �     �          .  ,   5     b     w     �     �  @   �  F   �  C   ,  V   p     �     �  D   �    (     @	     S	     l	                                                                               
   	                      Attributes Display your contact form in a widget. Email Enter number %s Guido van der Leest IP: %s Info about attributes Information Installation Message Name Please enter a valid email Please enter at least 10 characters Please enter at least 2 characters Please enter the correct number Subject Submit Thank you! You will receive a response as soon as possible. This is a very simple contact form. Use shortcode [contact] to display form on page or use the widget. For more info please check readme file. Title Very Simple Contact Form http://www.guidovanderleest.nl PO-Revision-Date: 2017-01-29 08:08:47+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: GlotPress/2.3.0-alpha
Language: ru
Project-Id-Version: Plugins - Very Simple Contact Form - Development (trunk)
 Атрибуты Виджет формы контактов E-mail Введите число %s Guido van der Leest IP: %s Как настроить параметры Информация Установка Сообщение Имя Пожалуйста, введите корректный e-mail Пожалуйста, введите хотя бы 10 символов Пожалуйста, введите хотя бы 2 символа Пожалуйста, введите число, как оно было указано Тема Отправить Спасибо! Мы ответим в ближайшее время Очень простая форма контактов. Чтобы вывести форму на странице, используйте шорткод [contact] или виджет. Дополнительную информацию можно найти в файле readme. Заголовок Very Simple Contact Form http://www.guidovanderleest.nl 